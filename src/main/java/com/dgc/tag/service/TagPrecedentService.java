package com.dgc.tag.service;

import com.dgc.dto.tag.TagInDTO;
import com.dgc.dto.tag.TagPrecedenteDTO;
import com.dgc.tag.domain.TagPrecedent;
import com.dgc.tag.repository.TagPrecedentRepository;
import com.dgc.tag.repository.TagRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class TagPrecedentService {

    @Autowired
    TagPrecedentRepository tagPrecedentRepository;

    @Autowired
    TagRepository tagRepository;

    public Set<TagPrecedenteDTO> guardarPrecedentes(Long tagId, Set<TagPrecedenteDTO> precedents ){
        Set<TagPrecedenteDTO> tagPrecedentesDTO = new HashSet<>();

        precedents.forEach( precedent ->{
            Set<TagPrecedent> alreadyAsignedPrecedent = tagPrecedentRepository.findByTagFatherIdTag(precedent.getTagPadre().getTagId());
            if ( alreadyAsignedPrecedent == null ) {
                TagPrecedent precedenteEntity = new TagPrecedent();
                precedenteEntity.setTagFather( tagRepository.findByTag( precedent.getTagPadre().getTag() ) );
                precedenteEntity.setTagSon( tagRepository.findByTag( precedent.getTagHijo().getTag() ) );
                precedenteEntity = tagPrecedentRepository.save( precedenteEntity );

                TagPrecedenteDTO tagPrecedenteDTO = new TagPrecedenteDTO();
                BeanUtils.copyProperties( precedenteEntity, tagPrecedenteDTO );
                tagPrecedentesDTO.add( tagPrecedenteDTO );
            }
        });

        return tagPrecedentesDTO;

    }

    void deletePrecedentesByPadreTagId (Long tagid){
        tagPrecedentRepository.deleteByTagFatherIdTag(tagid);
    }

    public Set<TagPrecedenteDTO> findSons(String tag){
        Set<TagPrecedent> entities = tagPrecedentRepository.findByTagFatherTag(tag);
        Set<TagPrecedenteDTO> retorno = null;
        TagPrecedenteDTO dto = null;
        TagInDTO tagPadreDTO = null;
        TagInDTO tagHijoDTO = null;
        if(entities != null && entities.size() > 0){
            retorno = new HashSet<>();

            for (TagPrecedent entity : entities){
                dto = new TagPrecedenteDTO();
                tagHijoDTO = new TagInDTO();
                tagPadreDTO = new TagInDTO();
                BeanUtils.copyProperties(entity.getTagSon(), tagHijoDTO);
                BeanUtils.copyProperties(entity.getTagFather(), tagPadreDTO);
                BeanUtils.copyProperties(entity, dto);
                dto.setTagHijo(tagHijoDTO);
                dto.setTagPadre(tagPadreDTO);
                retorno.add(dto);
            }
        }

        return retorno;
    }


    public TagPrecedenteDTO saveDto(TagPrecedenteDTO param){
        TagPrecedent precedente = new TagPrecedent();
        precedente.setTagFather( tagRepository.findByTag( param.getTagPadre().getTag() ) );
        precedente.setTagSon( tagRepository.findByTag( param.getTagHijo().getTag() ) );
        precedente = tagPrecedentRepository.saveAndFlush(precedente);
        TagPrecedenteDTO result = new TagPrecedenteDTO();
        BeanUtils.copyProperties(precedente, result);

        return result;

    }

    public void eliminarPrecedente(Long tagPrecedenteId){
        tagPrecedentRepository.deleteById( tagPrecedenteId );
    }

    public Set<TagPrecedenteDTO> findPadres(Long tagId){
        Set<TagPrecedenteDTO> padresDTO = null;
        Set<TagPrecedent> padres = tagPrecedentRepository.findByTagSonIdTag(tagId);

        padres.forEach( padre -> {
            TagPrecedenteDTO tagPrecedenteDTO = new TagPrecedenteDTO();
            BeanUtils.copyProperties(padre, tagPrecedenteDTO);
            padresDTO.add( tagPrecedenteDTO );
        });

        return padresDTO;

    }

}
