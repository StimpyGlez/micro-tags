package com.dgc.tag.service;

import com.dgc.dto.ResponseDTO;
import com.dgc.dto.tag.TagActividadOutDTO;
import com.dgc.dto.tag.TagInDTO;
import com.dgc.dto.tag.TagOutDTO;
import com.dgc.dto.tag.TagPrecedenteDTO;
import com.dgc.tag.domain.Tag;
import com.dgc.tag.repository.TagRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TagService {

    @Autowired
    TagRepository tagRepository;

    @Autowired
    TagActivityService tagActivityService;

    @Autowired
    TagPrecedentService tagPrecedentService;

    public TagInDTO saveTag(TagInDTO param ) {

        Tag entity = tagRepository.findByTag( param.getTag() );

        if ( entity == null ) {
            entity = new Tag();
        }

        BeanUtils.copyProperties( param, entity);

        //Guarda entidad principal
        entity = tagRepository.save( entity );
        param.setTagId( entity.getIdTag() );

        //Guarda precedentes
        if ( param.getPrecedentes() != null && param.getPrecedentes().size() > 0 ) {
            Set<TagPrecedenteDTO> precedentesDTO = tagPrecedentService.guardarPrecedentes( entity.getIdTag(), param.getPrecedentes() );
            param.setPrecedentes( precedentesDTO );
        } else {
            //Si no trea precedentes significa que se eliminaron los asignados
            tagPrecedentService.deletePrecedentesByPadreTagId( entity.getIdTag() );
        }

        tagActivityService.aumentarConsecutivo( param.getActividadId() );

        return param;
    }


    public List<TagInDTO> getAll() {
        List<Tag> entities = tagRepository.getAllTags();
        List<TagInDTO> retorno = new ArrayList<>();
        if ( entities != null && entities.size() > 0 ) {
            TagInDTO tagDTO = null;

            for (Tag entity : entities){
                tagDTO = new TagInDTO();
                BeanUtils.copyProperties( entity, tagDTO );
                tagDTO.setPrecedentes( tagPrecedentService.findSons( tagDTO.getTag() ) );
                //tagDTO.setPlantas( tagPlantaService.obtenerPlantasPorTagId( tagDTO.getTagId() ) );
                retorno.add( tagDTO );
            }
        }

        return retorno;
    }

    public TagInDTO getByTag(String tag ) {
        TagInDTO retorno = new TagInDTO();
        Tag entity = tagRepository.findByTag( tag );
        if ( entity != null ) {
            BeanUtils.copyProperties( entity, retorno );
            retorno.setPrecedentes( tagPrecedentService.findSons( entity.getTag() ) );
            //tagPlantaService.obtenerPlantasPorTagId( entity.getTagId() )
            //retorno.setPlantas( tagPlantaService.obtenerPlantasPorTagId( entity.getTagId() ) )
        }

        return retorno;
    }


    public TagInDTO obtenTagPorId( long idTag ) {
        TagInDTO retorno = new TagInDTO();
        Optional entity = tagRepository.findById( idTag );
        if ( entity.isPresent()) {
            Tag tag = (Tag) entity.get();
            BeanUtils.copyProperties( tag, retorno );
            retorno.setPrecedentes( tagPrecedentService.findSons( tag.getTag() ) );
            //tagPlantaService.obtenerPlantasPorTagId( entity.getTagId() )
            //retorno.setPlantas( tagPlantaService.obtenerPlantasPorTagId( entity.getTagId() ) )
        }

        return retorno;
    }


    public Set<TagInDTO> getActividadesPrecedentes( String tagFather) {
        Set<TagInDTO> listaTagsResult = new HashSet<>();
        Set<Tag> listaTags = new HashSet<>();
        Tag tagOwner = tagRepository.findByTag( tagFather );

        //Obtiene los hijos asociados
        Set<TagPrecedenteDTO> hijos = tagPrecedentService.findSons( tagFather );

        //Si no hay hijos, obtener los tags diferentes al owner
        if ( hijos == null ) {
            String[] tagFatherArray = {""};
            tagFatherArray[0]=tagFather;
            listaTags = tagRepository.findByTagNotIn( tagFatherArray );
        } else {
            //Si tiene hijos, mostrar las tags que no son sus hijos ni el owner
            listaTags = tagRepository.obtenerNoHijosNoOwner( tagOwner.getIdTag() );
        }

        TagInDTO tagDTO;
        for (Tag entity : listaTags){
            tagDTO = new TagInDTO();
            BeanUtils.copyProperties( entity, tagDTO );
            listaTagsResult.add( tagDTO );
        }

        return listaTagsResult;

    }


    public Set<TagPrecedenteDTO> addPrecedents(String tagPadre, String[] tagsAgregar ) {
        Tag padre = tagRepository.findByTag( tagPadre );
        TagInDTO padreDTO = new TagInDTO();
        BeanUtils.copyProperties( padre, padreDTO );

        for (String tag : tagsAgregar){
            TagInDTO nuevaAsignadaDTO = new TagInDTO();
            Tag nuevaAsignada = tagRepository.findByTag( tag );
            BeanUtils.copyProperties( nuevaAsignada, nuevaAsignadaDTO );
            TagPrecedenteDTO nuevoHijoDTO = new TagPrecedenteDTO();
            nuevoHijoDTO.setTagHijo( nuevaAsignadaDTO );
            nuevoHijoDTO.setTagPadre( padreDTO );
            tagPrecedentService.saveDto( nuevoHijoDTO );
        }

        Set<TagPrecedenteDTO> nuevosTagPrecedentesDTO = tagPrecedentService.findSons( tagPadre );

        return nuevosTagPrecedentesDTO;

    }

    public Set<TagPrecedenteDTO> deletePrecedent( TagPrecedenteDTO tagPrecedente ) {
        tagPrecedentService.eliminarPrecedente( tagPrecedente.getTagPrecedenteId() );
        Set<TagPrecedenteDTO> tagPrecedentesDTO = tagPrecedentService.findSons( tagPrecedente.getTagPadre().getTag() );

        return tagPrecedentesDTO;

    }

    public ResponseDTO deleteTag(Long tagId) {
        ResponseDTO resultado ;
        Set<TagPrecedenteDTO> tagPrecedenteDTO;
        Set<TagPrecedenteDTO> tagPadres;
        Optional tagEntity = tagRepository.findById(tagId);
        if ( tagEntity.isPresent() ){
            Tag tag = (Tag) tagEntity.get();
            tagPrecedenteDTO = tagPrecedentService.findSons( tag.getTag() );
            tagPadres = tagPrecedentService.findPadres(tagId);
            if ( tagPrecedenteDTO != null && tagPrecedenteDTO.size() > 0 ){
                resultado = new ResponseDTO(99, "Error","No es posible eliminar el Tag, debido a que tiene tags asignados.");
            }else if( tagPadres != null && tagPadres.size() > 0){
                resultado = new ResponseDTO(99, "Error","No es posible eliminar el Tag, debido a que ya está asignado.");
            }
            else{
                TagActividadOutDTO tagActividad = tagActivityService.obtenerActividad( tag.getIdActivity());
                Long consecutivo = Long.valueOf( tag.getTag().substring(tag.getTag().lastIndexOf('-')+1, tag.getTag().length() ));
                tagRepository.delete(tag);
                if ( tagActividad.getConsecutivo() == consecutivo){
                    //Es el último, se decrementa el consecutivo en actividad
                    tagActivityService.decrementarConsecutivo(tagActividad.getActividadId());
                }

                resultado = new ResponseDTO(0, "Success" ,"Tag eliminado" );
            }
        }else{
            resultado = new ResponseDTO(99, "Error","No existe el tag a eliminar.");
        }

        return resultado;
    }


    public Set<TagOutDTO> findByIdTypeComplianceAndIdActivity(Long maestroOpcionId, Long actividadId){
        Set<Tag> tags = tagRepository.findByIdTypeComplianceAndIdActivity(maestroOpcionId, actividadId);
        Set<TagOutDTO> tagsOut = new HashSet<>();
        tags.forEach( tag -> {
            TagOutDTO tagOutDTO = new TagOutDTO();
            BeanUtils.copyProperties(tag, tagOutDTO);
            tagsOut.add(tagOutDTO);
        });

        return tagsOut;
    }

}
