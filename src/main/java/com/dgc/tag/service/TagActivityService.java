package com.dgc.tag.service;

import com.dgc.dto.ResponseDTO;
import com.dgc.dto.estatusmaestro.EntidadEstatusDTO;
import com.dgc.dto.tag.TagActividadInDTO;
import com.dgc.dto.tag.TagActividadOutDTO;
import com.dgc.tag.domain.TagActivity;
import com.dgc.tag.repository.TagActivityRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TagActivityService {

    @Autowired
    TagActivityRepository tagActivityRepository;

    public TagActividadOutDTO aumentarConsecutivo(Long actividadId){

        TagActivity tagActivity = tagActivityRepository.findByIdActivity( actividadId );
        tagActivity.setConsecutive ( tagActivity.getConsecutive()+1 );
        tagActivity = tagActivityRepository.save( tagActivity );

        TagActividadOutDTO tagActividadOutDTO = new TagActividadOutDTO();
        if (tagActivity != null) {
            BeanUtils.copyProperties( tagActivity, tagActividadOutDTO );
        }

        return tagActividadOutDTO;
    }

    public TagActividadOutDTO obtenerActividad( Long actividadId){
        TagActividadOutDTO tagActividadOutDTO = null;
        TagActivity tagActividad = tagActivityRepository.findByIdActivity( actividadId );
        if (tagActividad != null) {
            tagActividadOutDTO = new TagActividadOutDTO();
            BeanUtils.copyProperties( tagActividad, tagActividadOutDTO );

            //TODO poner el estatus
            tagActividadOutDTO.setEstatus( null );
        }

        return tagActividadOutDTO;

    }

    TagActivity decrementarConsecutivo(Long tagActividadId){
        TagActivity tagActividad = tagActivityRepository.findByIdActivity(tagActividadId);
        tagActividad.setConsecutive(tagActividad.getConsecutive()-1);
        tagActividad = tagActivityRepository.save( tagActividad );
        return tagActividad;
    }

    //Agrega una nueva actividad con su prefijo y consecutivo = 1
    public TagActividadInDTO createActivity(TagActividadInDTO tagActividadInDTO ) {
        TagActivity tagActividadExiste = tagActivityRepository.findByNameOrPrefix(tagActividadInDTO.getNombre(), tagActividadInDTO.getPrefijo() );
        //EntidadEstatusDTO entidadEstatusActivo = entidadEstatusFeign.obtenEntidadEstatus( "CAT_ACTIVIDAD","Activo" )
        if ( tagActividadExiste == null ) {
            TagActivity tagActividad = new TagActivity();
            tagActividad.setName( tagActividadInDTO.getNombre() );
            tagActividad.setPrefix( tagActividadInDTO.getPrefijo() );
            tagActividad.setConsecutive( 0L );
            //TODO poner estatus
            tagActividad.setActive( true );
            tagActividad = tagActivityRepository.save( tagActividad );
            BeanUtils.copyProperties( tagActividad, tagActividadInDTO);
        }else{
            return null;
        }

        return tagActividadInDTO;
    }

    public TagActividadInDTO editActivity( TagActividadInDTO tagActividadInDTO ) {
        TagActivity tagActividadExiste = tagActivityRepository.findByIdActivity(tagActividadInDTO.getActividadId() );

        if ( tagActividadExiste != null) {
            tagActividadExiste.setName( tagActividadInDTO.getNombre() );
            tagActividadExiste.setPrefix( tagActividadInDTO.getPrefijo() );
            //TODO poner estatus
            tagActividadExiste.setActive( true );
            tagActividadExiste.setTaskOvercome( tagActividadInDTO.getTareaPorVencer() );
            tagActividadExiste.setTaskNextOvercome( tagActividadInDTO.getTareaProximaVencer() );
            tagActividadExiste.setTimeTask( tagActividadInDTO.getTareaTiempo() );

            tagActivityRepository.save( tagActividadExiste );
            BeanUtils.copyProperties( tagActividadExiste, tagActividadInDTO);
        }

        return tagActividadInDTO;

    }

    public ResponseDTO deleteActivity(Long actividadId){
        ResponseDTO resultado;
        TagActivity tagActividadExiste = tagActivityRepository.findByIdActivity(actividadId);

        if (tagActividadExiste != null ){
            if ( tagActividadExiste.getConsecutive() > 0 ){
                resultado = new ResponseDTO(99, "Error","No es posible eliminar la actividad, debido a que tiene folios asignados.");
            }else{
                tagActivityRepository.delete( tagActividadExiste );
                resultado = new ResponseDTO(0, "Success" ,"Actividad eliminada con éxito" );
            }
        }else{
            resultado = new ResponseDTO(99, "Error","No existe actividad a eliminar.");
        }

        return resultado;
    }

    public List<TagActividadOutDTO> getAll(String status){

        List <TagActivity> listaTagActividades = new ArrayList<>();

        if (status == null || status.equals( "TODOS" )){
            listaTagActividades = tagActivityRepository.getAllActivities();
        }else if ( status.equals( "ACTIVOS" )) {
            //TODO obtener por activos
            listaTagActividades = tagActivityRepository.getAllActivitiesActives(1L);
        }

        List <TagActividadOutDTO> listatagActividadesDTO = new ArrayList<>();
        listaTagActividades.forEach( tagActivity -> {
            TagActividadOutDTO tagActividadOutDTO = new TagActividadOutDTO();
            BeanUtils.copyProperties( tagActivity, tagActividadOutDTO);
            //TODO pone estatus
            tagActividadOutDTO.setEstatus( null );
            listatagActividadesDTO.add(tagActividadOutDTO);
        });

        return listatagActividadesDTO;

    }

    public TagActividadOutDTO getConsecutive(Long actividadId){
        TagActividadOutDTO tagActividadOutDTO = null;
        TagActivity tagActividad = tagActivityRepository.findByIdActivity( actividadId );
        if (tagActividad != null) {
            tagActividadOutDTO = new TagActividadOutDTO();
            BeanUtils.copyProperties( tagActividad, tagActividadOutDTO );
            tagActividadOutDTO.setSiguienteCodigoTag("" + tagActividad.getPrefix() + "-" + (tagActividad.getConsecutive()+1));
        }

        return tagActividadOutDTO;

    }


}
