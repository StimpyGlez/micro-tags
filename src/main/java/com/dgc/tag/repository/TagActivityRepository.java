package com.dgc.tag.repository;

import com.dgc.tag.domain.TagActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TagActivityRepository extends JpaRepository<TagActivity, Long> {

    @Query("FROM TagActivity T ORDER BY T.name ")
    public List<TagActivity> getAllActivities();

    @Query("FROM TagActivity T WHERE T.active = :active ORDER BY T.name ")
    public List<TagActivity> getAllActivitiesActives(@Param("active") Long active);

    public TagActivity findByNameOrPrefix(String name, String prefix );

    public TagActivity findByName(String name );

    public TagActivity findByIdActivity(Long idActivity);

}