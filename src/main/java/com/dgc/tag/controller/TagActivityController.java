package com.dgc.tag.controller;

import com.dgc.dto.GenericResponseDTO;
import com.dgc.dto.ResponseDTO;
import com.dgc.dto.tag.TagActividadInDTO;
import com.dgc.dto.tag.TagActividadOutDTO;
import com.dgc.tag.service.TagActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/tag")
@Transactional
public class TagActivityController {

    @Autowired
    TagActivityService tagActivityService;

    @CrossOrigin(origins = "*")
    @PostMapping("/actividad/crear")
    public ResponseEntity createActivty(@RequestBody TagActividadInDTO tagActividadInDTO) {
        GenericResponseDTO<TagActividadInDTO> genericResponseDTO = new GenericResponseDTO<>();
        TagActividadInDTO tagActividad = tagActivityService.createActivity(tagActividadInDTO);
        if ( tagActividad != null ) {
            genericResponseDTO.setClave( 0 );
            genericResponseDTO.setMensaje( "Creado correctamente" );
            genericResponseDTO.setEntity( tagActividad );
            return new ResponseEntity( genericResponseDTO, HttpStatus.OK );
        }else{
            genericResponseDTO.setClave( 99 );
            genericResponseDTO.setMensaje( "Ya existe la actividad o el prefijo." );
            return new ResponseEntity( genericResponseDTO, HttpStatus.OK );
        }
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/actividad/editar")
    public TagActividadInDTO editActivity( @RequestBody TagActividadInDTO tagActividadInDTO) {
        return tagActivityService.editActivity(tagActividadInDTO);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("actividad/eliminar/{actividadId}")
    public ResponseDTO deleteActivity(@PathVariable("actividadId") Long actividadId) {
        return tagActivityService.deleteActivity(actividadId);
    }


    @CrossOrigin(origins = "*")
    @GetMapping("/actividad/all/{status}")
    public List<TagActividadOutDTO> getAll(@PathVariable("status") String status){
        return tagActivityService.getAll(status);
    }


    @CrossOrigin(origins = "*")
    @GetMapping("/actividad/obtenerConsecutivo/{actividadId}")
    public TagActividadOutDTO getConsecutive(@PathVariable("actividadId") Long actividadId){
        return tagActivityService.getConsecutive(actividadId);
    }


    @CrossOrigin(origins = "*")
    @GetMapping("/actividad/obtenerActividad/{actividadId}")
    public TagActividadOutDTO getActivity(@PathVariable("actividadId") Long actividadId){
        return tagActivityService.obtenerActividad(actividadId);
    }

}
