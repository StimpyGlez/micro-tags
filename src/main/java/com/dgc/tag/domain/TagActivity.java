package com.dgc.tag.domain;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CAT_ACTIVITY")
public class TagActivity extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ACTIVITY")
    private Long idActivity;

    @Column(name = "NAME", unique = true)
    private String name;

    @Column(name = "PREFIX", unique = true, nullable = false)
    private String prefix;

    @Column(name = "CONSECUTIVE")
    private Long consecutive;

    @Column(name = "TIME_TASK")
    private Integer timeTask;

    @Column(name = "TASK_OVERCOME")
    private Integer taskOvercome;

    @Column(name = "TASK_NEXT_OVERCOME")
    private Integer taskNextOvercome;

    @Column(name = "ACTIVE")
    private boolean active;

}
