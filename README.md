# micro-tags

Microservicio encargado de la implementación e TAGs y Actividades

El microservicio está generado con Spring Boot 	version '2.1.6.RELEASE'
Java: 1.8

Se conecta a una base de datos Oracle (Al conectarse genera sus tablas)
Utiliza Lombok
swagger2:2.9.2
DiscoveryClient: openfeign


Es necesario levantar consul para registrar el servicio (Descargado de docker si se quiere probar, si no, deshabiliar consul)
Ejecución para levantar consul con docker: docker run --name consul -p 8500:8500 consul agent -dev -ui -client=0.0.0.0 -bind=0.0.0.0

Implementa multitenencia para base de datos.